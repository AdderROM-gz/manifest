<img src="https://i.imgur.com/89hGVic.png" height="50%" width="50%;"/>
  
# AdderROM (GZOSP Modified) #
  
[GZOSP Source](https://github.com/GZOSP) / [GZOSP Forum](https://forum.xda-developers.com/ground-zero-roms) / [Android Builders Collective](https://forum.xda-developers.com/custom-roms/android-builders-collective/rom-builders-collective-t2861778)  
  
### About: ###
This is a version of GZOSP that I'm tinkering with to learn more about the internal workings of Android P.  Don't expect it to work.
  
### Sync: ###
```bash
repo init -u https://gitlab.com/AdderROM/manifest -b 9.0
repo sync --force-sync -c --no-clone-bundle --no-tags --optimized-fetch --prune
```

